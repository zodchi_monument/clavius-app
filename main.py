# Standard library imports
from time import time
import argparse
import os

# Local application imports
os.chdir(os.path.dirname(os.path.abspath(__file__)))
from bin.classes.inout import StdinIO, FileIO

# Обработка аргументов
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--initmess", help="if TRUE - displays init message", type=bool, default=False)
parser.add_argument("-f", "--filename", help="if set - runs data from file ones", type=str)
parsed = parser.parse_args()

# Инициализация моделей
t = time()
file_name = parsed.filename
io = FileIO(file_name) if file_name else StdinIO()
if parsed.initmess:
    print(f'Initialization complete. Init time {time()-t} sec')

# Запуск сервиса
io.run()
