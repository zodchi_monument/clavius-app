FROM python:3.7 AS downloader
# configure download
ARG SPV_VERSION=0.1.4
ARG ART_USER
ARG ART_PASS
ENV ART_REPO=https://services.crplab.ru/artifactory/buldozer/${SPV_VERSION}/supervisor-linux-amd64
# download supervisor binary
RUN wget --http-user ${ART_USER} --http-password ${ART_PASS} -O /opt/supervisor "${ART_REPO}"
RUN chmod +x /opt/supervisor

# final image
FROM python:3.6.5
COPY --from=downloader /opt/supervisor /opt/supervisor
# setup supervisor as entrypoint
ENTRYPOINT ["/opt/supervisor"]
# (call python with main script and provide it ref to model file which is expected to be mounted at /var/model)
CMD ["--bin", "python3", "--args", "/opt/app/scripts/stdin_io.py,/var/model"]
# py env setup
ENV PYTHONPATH "/opt/app:${PYTHONPATH}"
EXPOSE 8080
# dependencies
COPY freeze.txt /opt/app/
RUN pip install -r /opt/app/freeze.txt
# version info
ARG PLATFORM_VERSION=unknown
ENV PLATFORM_VERSION $PLATFORM_VERSION
# sources
COPY / /opt/app
