# Standard library imports
from functools import wraps
import warnings
import json
import sys
import os

# Third party import

# Local application imports
from yaml import safe_load, YAMLError
from sklearn.externals import joblib
import pandas as pd
import numpy as np
from sklearn.covariance import EllipticEnvelope
from abc import abstractmethod


class Processor:

    def __init__(self):
        self._df = pd.DataFrame()

    @property
    def data(self):
        return self._df

    @data.setter
    def data(self, set_data):
        self._df = set_data

    @abstractmethod
    def run(self, df, mode):
        raise NotImplementedError


class Model(Processor):

    def __init__(self, conf):
        Processor.__init__(self)

    def run(self, df, mode):
        raise NotImplementedError


class PostProcessor(Processor):

    def __init__(self, conf):
        Processor.__init__(self)

    def run(self, df, mode):
        raise NotImplementedError


class PreProcessor(Processor):

    def __init__(self, conf):
        Processor.__init__(self)
        self._init_lattice(conf['lattice'])
        self._anomaly = bool(conf['anomaly'])
        self._scaler_path = conf['scaler']
        self._scaler = joblib.load(self._scaler_path)
        self._init_chains()

    def _init_lattice(self, lattice):
        self._lattice = lattice
        self._lattice['dinamic'] = [[int(j) for j in i.split(':')] for i in lattice['dinamic'].split(',')]

    def _init_chains(self):
        self.chains = {'fit': [self._norm_lattice, self._interpolate, self._smooth_anomaly,
                               self._dynamic_lattice, self._scale],
                       'forecast': [self._merge, self._norm_lattice, self._interpolate,
                                    self._smooth_anomaly, self._scale]}

    def _merge(self, df):
        self._df = df # HINT:вместо этой строки будет склейка данных
        return self._df

    def _norm_lattice(self, df):
        rows = df.loc[df.shape[0] - 1, 0]
        lattice = pd.Series([i for i in range(0, rows, self._lattice['raw'])])
        lattice = lattice.rename(df.columns[0])
        df = pd.merge(lattice, df, how='left', on=df.columns[0])
        return df.loc[:, 1:]

    @staticmethod
    def _interpolate(df):
        for i in range(df.shape[1]):
            df.iloc[:, i] = df.iloc[:, i].interpolate(method='polynomial', order=5)
            df.iloc[:, i] = df.iloc[:, i].interpolate(limit_direction='both')
        return df

    def _smooth_anomaly(self, df):
        if self._anomaly:
            _params = dict(contamination=0.02)
            _anomalies = EllipticEnvelope().fit_predict(np.array(df))
            _df_with_anomalies = df.copy()

            for i in range(np.shape(_anomalies)[0]):
                if _anomalies[i] == -1:
                    df.iloc[i, :] = None

            self._interpolate(df)
            for i in range(df.shape[1]):
                df.iloc[:, i] = (df.iloc[:, i] + _df_with_anomalies.iloc[:, i]) / 2
            return df

    def _scale(self, df):
        self._scaler.fit(df)
        joblib.dump(self._scaler, self._scaler_path)
        return pd.DataFrame(self._scaler.transform(df))

    def _dynamic_lattice(self, df):
        result = df
        # Тут короче пока не работает,п отому что формат поменял немного. Будет время подправлю
        """
        l = self._lattice['dinamic']
        start = l[1][0]
        result = df.iloc[:start, :]
        for i in range(1, len(l)):
            d = df.iloc[l[i - 1][]:l[i], :]
            z = d.groupby(d.index // m[i - 1]).mean()
            result = pd.concat([df, z])
        result.index = [i for i in range(result.shape[0])]
        """
        return result

    def run(self, df, mode):
        chain = self.chains[mode]
        for method in chain:
            df = method(df)
        return df


class ForecastModel:

    def __init__(self, conf):
        self.id = conf["id"]
        self._preprocessor = PreProcessor(conf['preprocessor'])
        self._processor = Model(conf['model'])
        self._postprocessor = PostProcessor(conf['postprocessor'])

    def run(self, data_jsn, mode):
        data = pd.DataFrame(data_jsn['values'])
        data = self._preprocessor.run(data, mode)
        #data = self._processor.run(data, mode)
        #data = self._postprocessor.run(data, mode)
        return data


class IO:

    def __init__(self):
        print(os.getcwd())
        self._configure()
        with open('config.yml', 'r') as yf:
            try:
                conf_dict = safe_load(yf)
            except YAMLError as exc:
                print(exc)
            self.models = {conf_dict[key]["id"]: ForecastModel(conf_dict[key]) for key in conf_dict}

    def _configure(self):
        """ Конфигурирование сервиса """
        os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
        warnings.filterwarnings("ignore")

    def _run_as_task(self, id, mode, data):
        print(self.models[id].run(data, mode))

    @abstractmethod
    def run(self):
        pass


class StdinIO(IO):

    def _configure(self):
        super()._configure()
        self._MAX_BUFFER_LINE_SIZE = 1048576

    def run(self):
        while True:
            task_json = sys.stdin.readline()
            task_json = json.loads(task_json)
            ident = self.models.get(task_json["id"]).id
            if ident in self.models:
                mode, data = task_json["mode"], task_json["data"]
                print(self._run_as_task(ident, mode, data))


class FileIO(IO):

    def __init__(self, file_name):
        super().__init__()
        self.file_name = file_name

    def run(self):
        for task_json in open(self.file_name, 'r'):
            task_json = json.loads(task_json)
            ident = self.models.get(task_json["id"])
            if ident in self.models:
                mode, data = task_json["mode"], task_json["data"]
                self._run_as_task(ident, mode, data)


if __name__ == '__main__':
    with open('config.yml', 'r') as yf:
        try:
            m = safe_load(yf)['model1']
        except YAMLError as exc:
            print(exc)
    task_json = sys.stdin.readline()
    task_json = json.loads(task_json)
    mode, data = task_json["mode"], task_json["data"]
    print(m.run(data, mode))
