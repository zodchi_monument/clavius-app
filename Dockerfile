FROM python:3.6.5
COPY freeze.txt /freeze.txt
WORKDIR /
RUN pip install -r freeze.txt
COPY . /
CMD ["python", "-u", "main.py"]
